\Sexpr{set_parent('WTC_Lysimeter_report.Rnw')}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% source files & libraries & setup options                                  %%
%% need to be loaded here if one wants to compile this child document to PDF %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% <<setup, include=FALSE>>=
% opts_chunk$set(concordance = TRUE, warning = FALSE, tidy = TRUE, tidy.opts = list(width.cutoff = 60))
% opts_knit$set(root.dir=normalizePath('../'))
% @
% 
% <<readFiles, include=FALSE>>=
% library(car)
% library(gmodels)
% library(lme4)
% library(lubridate)
% library(MASS)
% library(nlme)
% library(packrat)
% library(plyr)
% library(reshape)
% library(xlsx)
% library(contrast)
% library(effects)
% library(ggplot2)
% library(xtable)
% library(gridExtra)
% 
% source("R//functions.R")
% load("Output/Data/WTC_lysimeter.RData")
% source("R/SummaryExlTable.R")
% source("R//Figs.R")
% @

\subsection{Total organic C}

%%% Temp %%%
\subsubsection{Temp trt}

\begin{figure}[!h]\label{figure:temp_LysimeterTOC}

\begin{center}

<<FigTempTOC, echo=FALSE, fig.height=3, fig.width=6>>=
TrtFg[["toc"]]
@

\caption{TOC in soil water at temperature treatments}
\end{center}
\end{figure}

<<TableTempTOC, echo=FALSE, results='asis'>>=
print(xtable(TrtSmmryTbl[["toc.shallow"]],
             caption = "Temp trt mean of TOC in soil water at a shallow layer", 
             label = "table:tempLysTOCS",
             NA.string = c("NA"),
             align = rep("l", 8)),
      caption.placement = "top", 
      include.rownames = FALSE,
      table.placement = "H")
print(xtable(TrtSmmryTbl[["toc.deep"]],
             caption = "Temp trt mean of TOC in soil water at a deep layer", 
             label = "table:tempLysTOCD",
             NA.string = c("NA"),
             align = rep("l", 8)),
      caption.placement = "top", 
      include.rownames = FALSE,
      table.placement = "H")
@

%%%%%%%%%%%%%
%%% Stats %%%
%%%%%%%%%%%%%
\clearpage
\paragraph{Stats}
\noindent

<<ReadScript_WTC_Lys_TOC, echo=FALSE, include=FALSE>>=
source("R/Stat_toc.R")
read_chunk("R/Stat_toc.R")
@


%%%%%%%%%%%%%
%% Summary %%
%%%%%%%%%%%%%

\paragraph{Shallow}
\noindent

<<Stat_WTC_Lys_TOC_S_Smmry, echo=TRUE, results='markup'>>=
@

\paragraph{Deep}
\noindent

<<Stat_WTC_Lys_TOC_D_Smmry, echo=TRUE, results='markup'>>=
@


% %%%%%%%%%%%%
% %% Detail %%
% %%%%%%%%%%%%
% 
% \paragraph{Shallow}
% \noindent
% 
% <<Stat_WTC_Lys_TOC_S, echo=TRUE, results='markup'>>=
% @
% 
% \paragraph{Deep}
% \noindent
% 
% <<Stat_WTC_Lys_TOC_D, echo=TRUE, results='markup'>>=
% @


%%% Chamber %%%
\clearpage
\subsubsection{Chamber}

\begin{figure}[!h]\label{figure:Ch_LysimeterTOC}

\begin{center}

<<FigChTOC, echo=FALSE, fig.height=6, fig.width=6>>=
ChFg[["toc"]]
@

\caption{Total organic C in soil water in each chamber}
\end{center}
\end{figure}

<<TableChTOC, echo=FALSE, results='asis'>>=
print(xtable(ChSmmryTbl[["toc.shallow"]][, 1:13],
             caption = "Chamber means for TOC in 
             soil water at a shallow layer",
             label = "table:chamber_LysTOCS",
             NA.string = "NA",
             align = rep("l", 14)),
      caption.placement = "top",
      include.rownames= FALSE,
      table.placement = "H",
      size = "small")
print(xtable(ChSmmryTbl[["toc.shallow"]][,c(1, 14:25)],
             NA.string = c("NA"),
             align = rep("l", 14)),
      include.rownames= FALSE,
      table.placement = "H",
      size = "small")
print(xtable(ChSmmryTbl[["toc.shallow"]][,c(1, 26:37)],
             NA.string = c("NA"),
             align = rep("l", 14)),
      include.rownames= FALSE,
      table.placement = "H",
      size = "small")

print(xtable(ChSmmryTbl[["toc.deep"]][, 1:13],
             caption = "Chamber means for TOC in 
             soil water at a deep layer",
             label = "table:chamber_LysTOCD",
             NA.string = c("NA"),
             align = rep("l", 14)),
      caption.placement = "top",
      include.rownames= FALSE,
      table.placement = "H",
      size = "small")
print(xtable(ChSmmryTbl[["toc.deep"]][,c(1, 14:25)],
             NA.string = c("NA"),
             align = rep("l", 14)),
      include.rownames= FALSE,
      table.placement = "H",
      size = "small")
print(xtable(ChSmmryTbl[["toc.deep"]][,c(1, 26:37)],
             NA.string = c("NA"),
             align = rep("l", 14)),
      include.rownames= FALSE,
      table.placement = "H",
      size = "small")
@
